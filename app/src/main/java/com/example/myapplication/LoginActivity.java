package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LoginActivity extends AppCompatActivity {

    EditText formEmail, formPassword;
    Button btnLogin;

    final String urlLogin = "https://git.arif.app/siantrian/public/api/login";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        formEmail = (EditText) findViewById(R.id.et_email);
        formPassword = (EditText) findViewById(R.id.et_password);
        btnLogin = (Button) findViewById(R.id.btn_login);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Email = formEmail.getText().toString();
                String Password = formPassword.getText().toString();

//                new LoginUser().execute(Email,Password);
                if(Email.isEmpty() || Password.isEmpty()){
                    Toast.makeText(LoginActivity.this,"user pass is null", Toast.LENGTH_SHORT).show();
                }else {
                    Intent dsb = new Intent(LoginActivity.this,MainActivity.class);
                    startActivity(dsb);
                    finish();
                }
            }
        });

    }


    public class LoginUser extends AsyncTask<String, Void, String>{
        @Override
        protected String doInBackground(String... strings) {
            String Email = strings[0];
            String Password = strings[1];

            OkHttpClient okHttpClient = new OkHttpClient();
            RequestBody formBody = new FormBody.Builder()
                    .add("email", Email)
                    .add("password", Password)
                    .build();

            Request request = new Request.Builder()
                    .url(urlLogin)
                    .post(formBody)
                    .build();

            Response response = null;
            try {
                response = okHttpClient.newCall(request).execute();
                if(response.isSuccessful()){
                    String result = response.body().string();
                    if(result.equalsIgnoreCase("login")){
                        Intent i = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(i);
                        finish();
                    }
                }else{
                    Toast.makeText(LoginActivity.this, "Email of Password False", Toast.LENGTH_SHORT).show();
                }
            }catch (Exception e){
//                Toast.makeText(LoginActivity.this,"GAGAL", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
            return null;
        }
    }
}
